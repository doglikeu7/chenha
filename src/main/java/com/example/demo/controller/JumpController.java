package com.example.demo.controller;

import com.example.demo.domain.Log;
import com.example.demo.domain.Request;
import com.example.demo.service.GetRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
public class JumpController {

    @Autowired
    private GetRequestService getRequestService;
    @Autowired
    private HttpServletRequest httpServletRequest;

    @RequestMapping(value = "/info")
    private String GetMessage(Model model) throws IOException {

        Request resultRequest = getRequestService.getResult(httpServletRequest);

        model.addAttribute("parameterMap",resultRequest.getParameterMap());
        model.addAttribute("headerMap", resultRequest.getHeaderMap());

        Cookie[] cookies = httpServletRequest.getCookies();

        Map cookieMap = new HashMap();
        if(cookies != null){
            for(Cookie cookie : cookies){
                String key = cookie.getName();
                String value = cookie.getValue();
                cookieMap.put(key, value);
            }
            model.addAttribute("CookieMap",cookieMap);
        }else{
            String cookieMessage = "there's no cookie at all.";
            model.addAttribute("CookieMessage",cookieMessage);
        }




        return "info";
    }
    @RequestMapping("/info/refresh")
    public String refresh(Model model){
        Log log = Log.getInstance();
        String LogMessage = log.getLogMessage();
        model.addAttribute("LogMessage",LogMessage);
        return "refresh";
    }


}
