package com.example.demo.domain;

public class Log
{
    private static final Log instance = new Log();
    private String logMessage;

    public String getLogMessage() {
        return logMessage;
    }

    public void setLogMessage(String logMessage) {
        this.logMessage = logMessage;
    }

    private Log() { }

    public static Log getInstance() {
        return instance;
    }


}
