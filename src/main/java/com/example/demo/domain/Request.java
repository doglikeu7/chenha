package com.example.demo.domain;
import javax.servlet.http.Cookie;
import java.util.List;
import java.util.Map;

public class Request {
    private Map<String,String[]> parameterMap;
    private Map<String,String> headerMap;

    public Map<String, String[]> getParameterMap() {
        return parameterMap;
    }

    public void setParameterMap(Map<String, String[]> parameterMap) {
        this.parameterMap = parameterMap;
    }

    public Map<String,String> getHeaderMap(){
        return headerMap;
    }

    public void setHeaderMap(Map<String, String> map) {
        this.headerMap = map;
    }

}
