package com.example.demo.Filter;

import com.example.demo.domain.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.filter.AbstractRequestLoggingFilter;

import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;

@Order(Ordered.HIGHEST_PRECEDENCE)
@WebFilter(filterName = "traceLogFilter", urlPatterns = "/info")
public class TraceLogFilter extends AbstractRequestLoggingFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(TraceLogFilter.class);

    private final String TRACE_ID = "requestId";// 线程追踪ID

    private final String BEGIN_TIME_MILLIS = "BEGIN_TIME_MILLIS";// 开始时间

    @Override
    protected void beforeRequest(HttpServletRequest request, String message) {
        MDC.put(TRACE_ID, UUID.randomUUID().toString().replaceAll("-", ""));
        MDC.put(BEGIN_TIME_MILLIS, String.valueOf(System.currentTimeMillis()));
    }

    @Override
    protected void afterRequest(HttpServletRequest request, String message) {
        Log log = Log.getInstance();

        log.setLogMessage(message+" 耗费时间"+(System.currentTimeMillis() - Long.parseLong(MDC.get(BEGIN_TIME_MILLIS)))+"ms");

    }

    @Override
    public void destroy() {
        MDC.remove(TRACE_ID);
    }

    @Override
    protected boolean isIncludePayload() {
        return true;
    }

    @Override
    protected boolean isIncludeQueryString() {
        return true;
    }


}