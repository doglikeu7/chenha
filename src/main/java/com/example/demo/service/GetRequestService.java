package com.example.demo.service;

import com.example.demo.domain.Request;

import javax.servlet.http.HttpServletRequest;

public interface GetRequestService {
    Request getResult(HttpServletRequest httpServletRequest);
}
