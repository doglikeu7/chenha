package com.example.demo.service;

import com.example.demo.domain.Request;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@Service
public class GetRequestServiceImpl implements GetRequestService {
    @Override
    public Request getResult(HttpServletRequest httpServletRequest) {
        Request request = new Request();
        request.setHeaderMap(getHeadersInfo(httpServletRequest));
        request.setParameterMap(httpServletRequest.getParameterMap());
        return request;
    }

    private Map<String, String> getHeadersInfo(HttpServletRequest request) {
        Map<String, String> map = new HashMap<String, String>();
        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }
        return map;

    }


}
